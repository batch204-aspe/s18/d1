
// name - parameter
function printName(name){
	console.log("My name is " + name );
};
// Data passed into a function invocation can be received by the function
// This is what we call an "Argument"
printName("MK");

function theDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};
theDivisibilityBy8(456);

//Variables can also be passed as an argument
let sampleVariable = 64;
theDivisibilityBy8(sampleVariable);

/*
	Mini Activity:
		1. Create a function which is able to receive data as an argument
			- This function should be able to receive the name of your favorite superhero
			- dislay the name of your favorite superhero in the console

*/

function myFavHero(hero){

	console.log("My Marvel superhero is " + hero);
};
myFavHero('Thor');
myFavHero('Iron Man');


function otherPrintName(name) {
	console.log("My name is " + name);
};

// Function calling without arguments will result to undefined parameters.
otherPrintName();

otherPrintName(['MArk', 'Jayson', 'Raf']);


// Multiple "ARGUMENTS" will correspond to the number of "Parameters" declared in a function in a succeeding order.
function createFullName(firstName, middleName, lastname){
	console.log(firstName + " " + middleName + " " + lastname);
};
createFullName("Juan", "Dela", "Cruz");
// "Juan" will be stored in the parameter "firstName";
// "Dela" will be stored in the parameter "middleName";
// "Cruz" will be stored in the parameter "lastName";

createFullName("Juan", "Dela"); // the last parameters will return as "undefined"
createFullName("Juan", "Dela", "Cruz", "Hello"); // The extra parameter that is not declared is cannot be return

// Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastname = "Smith";
createFullName (firstName, middleName, lastname);

/*
	Mini Activity:
		2. Create a Function which will be able to receive 5 arguments
			- Receives 5 of your Fav SOngs
			- Display/print the passed 5 favorite songs in the console
				when the function is invoked
*/

function myTopSongs(song){
	console.log ("My Top 5 Songs");
	console.log(" 1. " + song[0] + "\n 2. " + song[1]  + "\n 3. " + song[2]  + "\n 4. " + song[3]  + "\n 5. " + song[4]);
};
myTopSongs(["Fire Flies", " Moanin ", " lullaby of Birdland ", " Favorite Things ", " Brave "]);

/*
	Return Statement
		► The "return" statement allows us to output a value from a function to be passed to the line/block of code that involed/called the function

		► Return STatement will be used if you declare it as a variable
	
*/

function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName
	console.log("Will this message be printed?");

}

let completeName = returnFullName("Jane", "Shatur", "Regina"); // declaring a variable of return function in order to invoked the return function.
console.log(completeName); // invoing the function

console.log(returnFullName("Jane", "Shatur", "Regina")); // or you can use this


function returnAddress(city, country){
	let fullAddress = city + " " + country;
	return fullAddress;
};
let myAddress = returnAddress("Manila ", "Philippines.");
console.log(myAddress);


/*
function printPlayerInfo (username, level, job){

	console.log("Username: " + username);
	console.log("Leve: " + level);
	console.log("job: " + job);

	
};
let user1 = printPlayerInfo("MK", 99, "Mage");

*/

function printPlayerInfo (username, level, job){

	return "username: " + username + ", level: " + level + ", Job: " + job;
	
};
let user1 = printPlayerInfo("MK", 99, "Mage");
console.log(user1);